#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "clogger.h"

#define COLOR_RED "\x1B[31m"
#define COLOR_BLUE "\x1B[34m"
#define COLOR_YELLOW "\x1B[33m"
#define COLOR_RESET "\x1B[0m"

static char * CLOGGER_LEVEL_DESCRIPTIONS[] = {"ERROR", "VERBOSE", "INFO"};
static char * CLLOGER_LEVEL_COLORS[] = {COLOR_RED, COLOR_BLUE, COLOR_YELLOW};

void write_log_to_stdout(const char *str);
void write_log_to_file(const char *str);
void create_log_directory_if_needed();

void clogger_log(CLOGGER_LEVEL level, const char *str, ...)
{
    char buffer[256];
    char buffer2[1024];
    char buffer3[1024];
    char time_msg[50];
    
    strftime(time_msg, sizeof(time_msg), "%T", localtime(&(time_t){time(NULL)}));
    
    va_list args;
    va_start(args, str);
    vsprintf(buffer, str, args);
    va_end(args);

    sprintf(buffer2, "[%s] %s[%s]%s %s\n", time_msg, CLLOGER_LEVEL_COLORS[level], CLOGGER_LEVEL_DESCRIPTIONS[level], COLOR_RESET, buffer);
    sprintf(buffer3, "[%s] [%s] %s\n", time_msg, CLOGGER_LEVEL_DESCRIPTIONS[level], buffer);

    create_log_directory_if_needed();

    write_log_to_file(buffer3);
    write_log_to_stdout(buffer2);   
}

void write_log_to_stdout(const char *str)
{
    printf("%s", str);
}

void write_log_to_file(const char *str)
{ 
    char filename[22];
    strftime(filename, sizeof(filename) , "./logs/%Y-%m-%d.log", localtime(&(time_t){time(NULL)}));

    FILE *fp = fopen(filename, "a+");
    if (!fp) return;

    fprintf(fp, "%s", str);

    fclose(fp);
}

void create_log_directory_if_needed()
{
    struct stat st = {0};
    if (stat("./logs", &st) == -1)
        mkdir("./logs", 0700);
}