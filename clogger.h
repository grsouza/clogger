#ifndef CLOGGER_H
#define CLOGGER_H

typedef enum {
    CLOGGER_LEVEL_ERROR,
    CLOGGER_LEVEL_VERBOSE,
    CLOGGER_LEVEL_INFO
} CLOGGER_LEVEL;

void clogger_log(CLOGGER_LEVEL level, const char *str, ...);

#endif