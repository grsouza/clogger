#include <stdio.h>
#include "clogger.h"

int main()
{
    clogger_log(CLOGGER_LEVEL_ERROR, "Not able to open file '%s'", "example.log");
    clogger_log(CLOGGER_LEVEL_INFO, "File opened: %d times", 5);
    clogger_log(CLOGGER_LEVEL_VERBOSE, "Verbose thing: %d", 123);
    return 0;
}