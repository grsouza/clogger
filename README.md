# Clogger
A tiny library for logging things in C

By default, the log is written to stdout and to a directory named "logs" under files with the format "Y-m-d.log".
Using this approach, the logs are separated by day.

The written log is in format "[TIME] [LEVEL] log string".

### Example
```c
clogger_log(CLOGGER_LEVEL_ERROR, "Not able to open file '%s'", "example.log");
// Output: [22:15:34] [ERROR] Not able to open file 'example.log'
```
